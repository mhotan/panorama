///////////////////////////////////////////////////////////////////////////
//
// NAME
//  FeatureAlign.h -- image registration using feature matching
//
// SEE ALSO
//  FeatureAlign.h      longer description
//
// Copyright ?Richard Szeliski, 2001.  See Copyright.h for more details
// (modified for CSE576 Spring 2005)
//
///////////////////////////////////////////////////////////////////////////

#include "ImageLib/ImageLib.h"
#include "FeatureAlign.h"
#include <math.h>
#include <cstdlib>
#include <algorithm>
#include <iostream>

// Number of potential inlier matches per sample
#define NUM_SAMPLES 1 

/******************* TO DO *********************
* alignPair:
*	INPUT:
*		f1, f2: source feature sets
*		matches: correspondences between f1 and f2
*               *NOTE* Each match in 'matches' contains two feature ids of matching features, id1 (in f1) and id2 (in f2).
*               These ids are 1-based indices into the feature arrays,
*               so you access the appropriate features as f1[id1-1] and f2[id2-1].
*		m: motion model
*		f: focal length
*		nRANSAC: number of RANSAC iterations
*		RANSACthresh: RANSAC distance threshold
*		M: transformation matrix (output)
*	OUTPUT:
*		repeat for nRANSAC iterations:
*			choose a minimal set of feature matches
*			estimate the transformation implied by these matches
*			count the number of inliers
*		for the transformation with the maximum number of inliers,
*		compute the least squares motion estimate using the inliers,
*		and store it in M
*/
int alignPair(const FeatureSet &f1, const FeatureSet &f2,
              const vector<FeatureMatch> &matches, MotionModel m, float f,
              int nRANSAC, double RANSACthresh, CTransform3x3& M)
{
    // BEGIN TODO
    // write this entire method

	// Create a cache for the best CTransform3x3
	vector<int> currBestIds;
	// must keep track of how many inliers matched
	int currBestInliers = 0;
	CTransform3x3 currentBestM;
	// for nRANSAC times
	for (int i = 0; i < nRANSAC; ++i){
		
		// Stores all of the index to the FeatureMatch that has been
		// added as a sample
		vector<int> tempMatchesIdxs;

		// Pick a random amount of NUM_SAMPLES matches and place
		// a new vector
		for (int j = 0; j < NUM_SAMPLES; ++j) {
			//Choose a random number to index the feature match
			int matchIdx = rand() % matches.size();
			// Check if feature has not been already selected
			// if so then find a new match Index
			while (std::find(tempMatchesIdxs.begin(),
				tempMatchesIdxs.end(), matchIdx) != tempMatchesIdxs.end()){
				matchIdx = rand() % matches.size();
			}

			//New match index found
			tempMatchesIdxs.push_back(matchIdx);
		}

		// Now the set of matches is complete up to NUM_SAMPLES
		if (tempMatchesIdxs.size() != NUM_SAMPLES){
			std::cerr << "Number of matches: " << tempMatchesIdxs.size() <<
				" is not equal to " << NUM_SAMPLES;
		}

		// Create a storage matrix to 
		CTransform3x3 M_temp;
		// Calculate the least square value
		// Store transformation matrix in M_temp

		leastSquaresFit(f1,f2,matches,m,f,tempMatchesIdxs,M_temp);

		// Count the number of inliers determine by motion model m
		int tempNumInliers = countInliers(f1,f2,matches,m,f,
			M_temp,RANSACthresh,tempMatchesIdxs);
		
		//NOTE! ensure tempMatchesIdxs has new Ids added to it

		// Check with current max and if it greater we have a better model
		if (tempNumInliers > currBestInliers){
			currBestInliers = tempNumInliers;
			currBestIds = tempMatchesIdxs;
			currentBestM = M_temp;
		}
	}

	// Now calculate the new M based off all of the inlier Ids
	leastSquaresFit(f1,f2,matches,m,f,currBestIds,M);
    // Now M stores the best fit model (homography)
	// END TODO

    return 0;
}

/******************* TO DO *********************
* countInliers:
*	INPUT:
*		f1, f2: source feature sets
*		matches: correspondences between f1 and f2
*               *NOTE* Each match contains two feature ids of matching features, id1 (in f1) and id2 (in f2).
*               These ids are 1-based indices into the feature arrays,
*               so you access the appropriate features as f1[id1-1] and f2[id2-1].
*		m: motion model
*		f: focal length
*		M: transformation matrix
*		RANSACthresh: RANSAC distance threshold
*		inliers: inlier feature IDs
*	OUTPUT:
*		transform the matched features in f1 by M
*
*		count the number of matching features for which the transformed
*		feature f1[id1-1] is within SSD distance RANSACthresh of its match
*		f2[id2-1]
*
*		store the indices of these matches in inliers
*
*		
*/
int countInliers(const FeatureSet &f1, const FeatureSet &f2,
                 const vector<FeatureMatch> &matches, MotionModel m, float f,
                 CTransform3x3 M, double RANSACthresh, vector<int> &inliers)
{
    inliers.clear();
    int count = 0;

    for (unsigned int i=0; i<(int) matches.size(); i++) {
        // BEGIN TODO
        // determine if the ith matched feature f1[id1-1], when transformed by M,
        // is within RANSACthresh of its match in f2
        //
        // if so, increment count and append i to inliers
        //
        // *NOTE* Each match contains two feature ids of matching features, id1 and id2.
        //        These ids are 1-based indices into the feature arrays,
        //        so you access the appropriate features as f1[id1-1] and f2[id2-1].

		// For each match in the matches
		const FeatureMatch& match = matches[i];
		const Feature& orig = f1[match.id1-1];
		const Feature& dest = f2[match.id2-1];

		// Store orig as a homogenous point
		CVector3 origHomo = CVector3();
		origHomo[0] = orig.x;
		origHomo[1] = orig.y;
		origHomo[2] = 1.0;

		// Calculate the projected matrix
		CVector3 projected = M * origHomo;

		// Calculate the difference between the projected and destination
		// Using the distance of line between two points equation
		// d = ((x2-x1)^2 + (y2-y1)^2) ^ 1/2
		double dist = sqrt(pow(projected[0]-dest.x, 2) + pow(projected[1]-dest.y, 2));

		// If below threshold increment count and add to inliers
		if (dist < RANSACthresh){
			++count;
			inliers.push_back(i);
		}

        // END TODO
    }

    return count;
}

/******************* TO DO *********************
* leastSquaresFit:
*	INPUT:
*		f1, f2: source feature sets
*		matches: correspondences between f1 and f2
*		m: motion model
*		f: focal length
*		inliers: inlier match indices (indexes into 'matches' array)
*		M: transformation matrix (output)
*	OUTPUT:
*		compute the transformation from f1 to f2 using only the inliers
*		and return it in M
*/
int leastSquaresFit(const FeatureSet &f1, const FeatureSet &f2,
                    const vector<FeatureMatch> &matches, MotionModel m, float f,
                    const vector<int> &inliers, CTransform3x3& M)
{
    // for project 2, the transformation is a translation and
    // only has two degrees of freedom
    //
    // therefore, we simply compute the average translation vector
    // between the feature in f1 and its match in f2 for all inliers
    double u = 0;
    double v = 0;

    for (unsigned int i=0; i < inliers.size(); i++) {
        double xTrans, yTrans;

        // BEGIN TODO
        // compute the translation implied by the ith inlier match
        // and store it in (xTrans,yTrans)

		const FeatureMatch& fMatch = matches[inliers[i]];

		//Retrieve the corresponding index(offset 1)
		// feature of the match
		const Feature& origFeature = f1[fMatch.id1 - 1];
		const Feature& otherFeature = f2[fMatch.id2 - 1];

		// offset of x = Feature from f2 - feature from f1
		// Therefore f1 + x or y = f2 x or y
		xTrans = (double)otherFeature.x - (double)origFeature.x;
		yTrans = (double)otherFeature.y - (double)origFeature.y;

        // END TODO

        u += xTrans;
        v += yTrans;
    }

    u /= inliers.size();
    v /= inliers.size();

    M[0][0] = 1;
    M[0][1] = 0;
    M[0][2] = u;
    M[1][0] = 0;
    M[1][1] = 1;
    M[1][2] = v;
    M[2][0] = 0;
    M[2][1] = 0;
    M[2][2] = 1;

    return 0;
}
