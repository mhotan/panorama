# Makefile for project 2

PROJ2=CSE576
PROJ2_OBJS=BlendImages.o FeatureSet.o FeatureAlign.o \
		WarpSpherical.o Project2.o

IMAGELIB=ImageLib/libImage.a

CC=g++
CPPFLAGS=-Wall -O3 -I/uns/include
LIB_PATH=-L/uns/lib -L/usr/X11R6/lib

all: $(PROJ2)

$(IMAGELIB): 
	make -C ImageLib

$(PROJ2): $(PROJ2_OBJS) $(IMAGELIB)
	$(CC) -o $@ $(PROJ2_OBJS) $(LIB_PATH) $(IMAGELIB)

clean:
	rm -f *.o *~ $(PROJ2)
