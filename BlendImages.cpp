///////////////////////////////////////////////////////////////////////////
//
// NAME
//  BlendImages.cpp -- blend together a set of overlapping images
//
// DESCRIPTION
//  This routine takes a collection of images aligned more or less horizontally
//  and stitches together a mosaic.
//
//  The images can be blended together any way you like, but I would recommend
//  using a soft halfway blend of the kind Steve presented in the first lecture.
//
//  Once you have blended the images together, you should crop the resulting
//  mosaic at the halfway points of the first and last image.  You should also
//  take out any accumulated vertical drift using an affine warp.
//  Lucas-Kanade Taylor series expansion of the registration error.
//
// SEE ALSO
//  BlendImages.h       longer description of parameters
//
// Copyright ?Richard Szeliski, 2001.  See Copyright.h for more details
// (modified for CSE455 Winter 2003)
//
///////////////////////////////////////////////////////////////////////////

#include "ImageLib/ImageLib.h"
#include "BlendImages.h"
#include <math.h>
#include <iostream>
#include <fstream>

static int cntr = 0;

/******************* TO DO 4 *********************
* AccumulateBlend:
*	INPUT:
*		img: a new image to be added to acc
*		acc: portion of the accumulated image where img is to be added
*		blendWidth: width of the blending function (horizontal hat function;
*	    try other blending functions for extra credit)
*	OUTPUT:
*		add a weighted copy of img to the subimage specified in acc
*		the first 3 band of acc records the weighted sum of pixel colors
*		the fourth band of acc records the sum of weight
*/
static void AccumulateBlend(CByteImage& img, CFloatImage& acc, float blendWidth)
{
	// *** BEGIN TODO ***
	// fill in this routine..

	CFloatImage imgF;
	CopyPixels(img, imgF);
	// MH Note from ADT analyzation
	// Sub image is Guaranteed to be within the height range of 
	// acc (accumulator)
	// Therefore just iterate through width of the blendwidth
	// add reach the end height of new img
	const CShape& img_shape = imgF.Shape();
	int numBands = img_shape.nBands;

	const int img_height = img_shape.height;
	const int img_width = img_shape.width;

	for (int y = 0; y < img_height; ++y)
	{ //For every row of pixels
		for (int x = 0; x < img_width; ++x)
		{ // Only blend up to the predefined width
			// if x does not exceed the blendwidth
			float imgAlphaF = imgF.Pixel(x, y, numBands-1);

			// Half to check Pixel on the image is complete black.
			// if so use 
			const uchar ZERO = 0;
			bool isBlack = true;
			for (int b = 0; b < numBands-1; ++b){
				isBlack &= img.Pixel(x, y, b) == ZERO;
			}

			// if the new imaeg pixel is black just revert to old image
			// pixel values
			if (isBlack) {
				// Do nothing because pixel value already exist in accumulator
			} else if (x < blendWidth){

				// Adjust using feathering weights
				const float slope = 1.0 / (float)blendWidth;
				const float imgWeight = ((float)x * slope);
				const float accWeight = 1 - imgWeight;

				// Add the 
				for (int b = 0; b < numBands-1; ++b)
				{ // For every band in the pixel accumulate the
					float imgPixColor = (float)imgF.Pixel(x, y, b);
					acc.Pixel(x, y, b) *= accWeight;
					acc.Pixel(x, y, b) += imgPixColor * imgAlphaF * imgWeight;
				}
				//  Add the alpha channel
				acc.Pixel(x, y, numBands-1) *= accWeight;
				acc.Pixel(x, y, numBands-1) += imgAlphaF * imgWeight;
			} else {
				// For every band no within the blendwidth
				for (int b = 0; b < numBands-1; ++b)
				{ // For every band in the pixel accumulate the
					float imgPixColor = (float)imgF.Pixel(x, y, b);
					// Change back to +=
					acc.Pixel(x,  y, b) =  imgPixColor*imgAlphaF;
				}
				acc.Pixel(x, y, numBands-1) = imgAlphaF;
			}
		}
	}

	// *** END TODO ***
}

/******************* TO DO 5 *********************
* NormalizeBlend:
*	INPUT:
*		acc: input image whose alpha channel (4th channel) contains
*		     normalizing weight values
*		img: where output image will be stored
*	OUTPUT:
*		normalize r,g,b values (first 3 channels) of acc and store it into img
*/
static void NormalizeBlend(CFloatImage& acc, CByteImage& img)
{
	// *** BEGIN TODO ***
	// fill in this routine..
	CShape shape = acc.Shape();
	int width = shape.width;
	int height = shape.height;
	int numBands = shape.nBands;

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x){
			// Alpha channel
			float alphaC = acc.Pixel(x, y, numBands-1);
			// For every image normalize and store in output image
			for (int b = 0; b < numBands-1; ++b)
			{ // For every band in the pixel accumulate the
				img.Pixel(x, y, b) = acc.Pixel(x, y, b) / alphaC;
			}
		}
	}

	// *** END TODO ***
}

/******************* TO DO 5 *********************
* BlendImages:
*	INPUT:
*		ipv: list of input images and their relative positions in the mosaic
*		blendWidth: width of the blending function
*	OUTPUT:
*		create & return final mosaic by blending all images
*		and correcting for any vertical drift
*/
CByteImage BlendImages(CImagePositionV& ipv, float blendWidth)
{

	// Assume all the images are of the same shape (for now)
	CByteImage& img0 = ipv[0].img;
	CShape sh        = img0.Shape();
	int width        = sh.width;
	int height       = sh.height;
	int nBands       = sh.nBands;
	int dim[2]       = {width, height};

	// Compute the bounding box for the mosaic
	int n = ipv.size();
	float min_x = 0, min_y = 0;
	float max_x = 0, max_y = 0;
	int i;
	for (i = 0; i < n; i++)
	{
		float* pos = ipv[i].position;

		// *** BEGIN TODO #1 ***
		// add some code here to update min_x, ..., max_y

		// NOTE: MH Assmptions
		// assumed that the first element in the list is the reference image
		// therefore min x and min y is the first element in the list
		float temp_x = pos[0];
		float temp_y = pos[1];

		// Assuming the image min is the top most left corner of the first image
		if (temp_x < min_x) min_x = temp_x;
		if (temp_y < min_y) min_y = temp_y;

		// Have account for he last image point does not account for the complete
		// size of the last image
		if (temp_x+width > max_x) max_x = temp_x+width;
		if (temp_y+height > max_y) max_y = temp_y+height;

		// *** END TODO #1 ***
	}
	// Create a floating point accumulation image
	CShape mShape((int)(ceil(max_x) - floor(min_x)),
		(int)(ceil(max_y) - floor(min_y)), nBands);
	CFloatImage accumulator(mShape);
	accumulator.ClearPixels();

	// Add in all of the images
	for (i = 0; i < n; i++)
	{
		// Compute the sub-image involved
		float* pos = ipv[i].position;
		int start_x = 0, start_y = 0;       

		// *** BEGIN TODO #2 ***
		// fill in with the correct values for start_x and start_y,
		// the relative position of i-th image in the mosaic
		// hint: we only require you to use pixel accuracy to start_x and start_y;

		// MH Assumption  
		/*

		<------Accumualtor-----kk-->
		<startx, starty>
		\/
		*---------*---------------*
		|||||||||||     |         |
		*---------*---------------*
		<complete> <img>
		*/
		// MH NOTE Check rounding issue form float to int
		int x0 = (int)pos[0] - (int)min_x;
		int y0 = (int)pos[1] - (int)min_y;
		start_x = x0;
		start_y = accumulator.Shape().height - (height + y0); 
		// *** END TODO #2 ***

		CByteImage& img = ipv[i].img;

		//Create a sub image the size of img,  Has all values of accumulator already
		CFloatImage sub = accumulator.SubImage(start_x, start_y, width, height);

		// Perform the accumulation
		AccumulateBlend(img, sub, blendWidth);
	}

	// Normalize the results
	CByteImage compImage(mShape);
	NormalizeBlend(accumulator, compImage);
	bool debug_comp = true;
	if (debug_comp)
		WriteFile(compImage, "tmp_comp.tga");

	// Allocate the final image shape
	CShape cShape(mShape.width - width, height, nBands);
	CByteImage croppedImage(cShape);

	// Compute the affine deformation
	CTransform3x3 A;

	// *** BEGIN TODO #3 ***
	// fill in the right entries in A to trim the left edge and
	// to take out the vertical drift

	float *lastImgPos = ipv[ipv.size()-1].position;
	float *firstImgPos = ipv[0].position;

	A[0][0] = 1;
	A[0][1] = 0;
	A[0][2] = 0; // Do not shift along X axis
	A[1][0] = 0;
	A[1][1] = 1;
	// The difference in the Y direction of the 
	A[1][2] = lastImgPos[1] - firstImgPos[1];
	A[2][0] = 0;
	A[2][1] = 0;
	A[2][2] = 1;

	// *** END TODO #3 ***

	// Warp and crop the composite
	WarpGlobal(compImage, croppedImage, A, eWarpInterpLinear);

	return croppedImage;
}
